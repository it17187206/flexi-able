import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'


class ID:
    def __init__(self, img):
        self._udid = ''
        self._name = ''
        self._father_name = ''
        self._dob = ''
        self._type_of_disability = ''
        self._img = img
        self.ocr()

    def get_udid(self):
        return self._udid

    def get_name(self):
        return self._name

    def get_father_name(self):
        return self._father_name

    def get_dob(self):
        return self._dob

    def get_type_of_disability(self):
        return self._type_of_disability

    def __str__(self):
        return 'UDID : ' + self._udid + ', Name : ' + self._name + ', Disability : ' + self._type_of_disability

    def ocr(self):
        text = pytesseract.image_to_string(self._img)
        list = text.split('\n')
        for i in list:
            if 'UDID Number' in i:
                self._udid = i.replace('UDID Number :', '').replace(' ', '')
            if ('Name' in i) and ('Father' not in i):
                self._name = i.replace('Name :', '')
            if "Father" in i:
                self._father_name = i.split(':')[1]
            if 'Date of Birth' in i:
                self._dob = i.replace('Date of Birth :', '').replace(' ', '')
            if 'Type' in i:
                self._type_of_disability = i.replace('Type of Disability :', '')






