import NIC
import cv2

# Read Image
img = cv2.imread('Data/sample_id.jpg')

# create id object
id_obj = NIC.ID(img)

# print str in object
print(id_obj)

# get data from getters
print('UDID number is ', id_obj.get_udid())
print('Member Name is', id_obj.get_name())
print('Date of Birthday is ', id_obj.get_dob())
print('Father Name is ', id_obj.get_father_name())
print('Disability type is ', id_obj.get_type_of_disability())